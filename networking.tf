module "networking" {
  source       = "./networking"
  project_name = local.project_name
  region = "europe-west2"
}