module "k8s-cluster" {
  source       = "./k8s-cluster"
  project_name = local.project_name
  network      = module.networking.network
  subnetwork   = module.networking.subnetwork
}