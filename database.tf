resource "google_sql_database" "reevedata_mysql_db" {
  instance = google_sql_database_instance.shared_db.name
  name = "reevedata_mysql_db"
}

resource "google_sql_database_instance" "shared_db" {
  name = "wordpress-shared"
  region = "europe-west2"
  database_version = "MYSQL_5_6"
  project = local.project_name
  settings {
    tier = "db-f1-micro"
    ip_configuration {
      private_network = module.networking.network_link
      ipv4_enabled = false
      require_ssl = false

    }
  }
}