import os

import diagrams
from diagrams import Diagram, Cluster, Edge, Node
from diagrams.gcp.storage import Storage
from diagrams.gcp.database import SQL
from diagrams.k8s.network import Ingress, Service
from diagrams.k8s.network import Service
from diagrams.gcp.compute import GKE
from diagrams.k8s.compute import Deployment
from diagrams.onprem.network import Nginx
from diagrams.onprem.compute import Server
from diagrams.onprem.client import User
from diagrams.custom import Custom
import os, sys

cloud_flare_logo = os.path.join(sys.path[0],"cloudflare.png")
gitlab_logo = os.path.join(sys.path[0],"gitlab.png")
docker_logo = os.path.join(sys.path[0],"dockerlogo.png")

with Diagram(name="reevedata-diagram", show=False, outformat="jpg",) as diagram:

    user = User()
    dns = Custom("cloudflare dns",icon_path=cloud_flare_logo)
    user >> Edge(label="\n\n\n\nYour browser requests") >> dns
    dns >> user

    with Cluster("K8s Cluster"):

        cluster = GKE("GKE Cluster")

        ingress = Nginx("Main Kubernetes Ingress")

        with Cluster("reevedata-wordpress (/*") as wp_blog:
            wp_blog = Service("Wordpress Blog")
            dns >> ingress >> Edge(color="black") >> wp_blog >> SQL("Wordpress Backend")
            dns << Storage("Static Content, images etc")

        with Cluster("Hello Web (/applications/helloweb)") as other_apps:

            helloweb = Deployment("helloweb app")

            dns >> ingress >> Edge(color="black") >> Service("helloweb") >> helloweb

        cluster

    with Cluster("Other Servers"):

        externals =  Server("External Hosted Applications")
        ingress >> Service() >> externals

    with Cluster("Development pipeline"):

        my_work = User("Local development")
        docker_registry = Custom("Docker Registry", icon_path=docker_logo)
        deployment_pipelines = Custom("Gitlab pipelines",icon_path=gitlab_logo)
        my_work >> Edge(label="\n\n\n\n\n\ncode") >> deployment_pipelines >> docker_registry >> cluster

