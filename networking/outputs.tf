output "network" {
  value = google_compute_network.main_network.name
}

output "subnetwork" {
  value = google_compute_subnetwork.subnet_1.name
}

output "network_link" {
  value = google_compute_network.main_network.self_link
}