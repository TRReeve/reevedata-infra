resource "google_compute_network" "main_network" {
  name                    = "${var.project_name}-network"
  auto_create_subnetworks = false
  project                 = var.project_name
}

locals {
  subnet_1_ip = "10.10.10.0/24"
}

resource "google_compute_subnetwork" "subnet_1" {
  name                     = "subnet-1"
  ip_cidr_range            = local.subnet_1_ip
  network                  = google_compute_network.main_network.name
  region                   = var.region
  private_ip_google_access = true
}