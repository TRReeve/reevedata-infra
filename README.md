Core Infrastructure for reevedata.com

##### Main Components
- Kubernetes Cluster (GKE)
- VPC Network 
- SQL Database for wordpress backend + Private networking

To Run

    terraform init
    terraform plan
    terraform apply
