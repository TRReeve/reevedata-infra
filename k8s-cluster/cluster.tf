resource "google_container_cluster" "gke_cluster" {
  name               = "${var.project_name}-k8s"
  initial_node_count = 1
  network            = var.network
  subnetwork         = var.subnetwork
  location           = "europe-west2-a"

  node_config {
    machine_type = "n1-standard-1"
    preemptible = true
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
    metadata = {
      disable-legacy-endpoints = "true"
    }
    labels = {
      project = var.project_name
      nodes = "admin-node"
    }
    tags = [var.project_name, "k8s-cluster-admin"]
  }

  ip_allocation_policy {
    cluster_ipv4_cidr_block  = "/16"
    services_ipv4_cidr_block = "/22"
  }

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

resource "google_container_node_pool" "primary_pool" {

  cluster = google_container_cluster.gke_cluster.name
  location           = "europe-west2-a"

  autoscaling {
    max_node_count = 2
    min_node_count = 1
  }

  node_config {

    machine_type = "n1-standard-2"
    preemptible = true
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
    metadata = {
      disable-legacy-endpoints = "true"
    }
    labels = {
      project = var.project_name
      nodes = "primarypool"
    }
    tags = [var.project_name, "k8s-cluster-nodepool"]
  }

}